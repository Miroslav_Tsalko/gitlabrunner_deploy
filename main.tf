terraform {
  backend "remote" {
    organization = "MirTs"

    workspaces {
      name = "custom_runner"
    }
  }
}

variable "AWS_SECRET_ACCESS_KEY" {}
variable "AWS_ACCESS_KEY_ID" {}
//
provider "aws" {
  region = "us-east-2"
}

resource "aws_instance" "Runner_Deploy" {
//Deb
  ami           = "ami-0a91cd140a1fc148a"
//Hat
//  ami           = "ami-0a0ad6b70e61be944"
 
  instance_type = "t2.micro" //Is A Free instance
  tags = {
    Name = "Gitlab Custom Runner"
  }
  key_name="AWS_for_runner"
  vpc_security_group_ids = [ aws_security_group.runner_security_group.id ]
  }
  
resource "aws_key_pair" "AWS" {
  key_name   = "AWS_for_runner"
  public_key = file("./Stuff/Key.pub")                  //give servers Public Key
}
resource "aws_security_group" "runner_security_group" {
  name        = "Runner Security Group"
  description = "To allow Web Access"

  dynamic "ingress" {
    for_each = ["80", "443", "22", "8080", "9000"] //Allow ports for Nginx, Jenkins, SSH
    content {
      from_port   = ingress.value
      protocol    = "tcp"
      to_port     = ingress.value
      cidr_blocks = ["0.0.0.0/0"] //allow to every IP  from outside network 
    }
  }

  egress {
    from_port   = 0 //Every traffic to every IP to outter network is avaliable
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
output "public_ip_for_Deploy" {     //output public IP for Deploy server
  value = aws_instance.Runner_Deploy.public_ip
}



